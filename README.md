Author: jared.vasquez@yale.edu

***

For first time users, try our [**Tutorial**](https://gitlab.cern.ch/jvasquez/TRTFramework/wikis/Tutorial).

Extensive documentation and guides can be found in our [wiki](https://gitlab.cern.ch/jvasquez/TRTFramework/wikis/home)

Be sure to use Analysis Release 2.4.X