#ifndef TRTUtils_H
#define TRTUtils_H

#include <iostream>
#include "TString.h"
#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "TRTFramework/TRTIncludes.h"

using TruthLink_t = ElementLink<xAOD::TruthParticleContainer>;

namespace TRT {

  static SG::AuxElement::Accessor<TruthLink_t> truthLink("truthParticleLink");
  
  //! \brief typedef for a vector of doubles (to save some typing)
  typedef std::vector<double>  NumV;
  //! \brief typedef for a vector of ints (to save some typing)
  typedef std::vector<int>     IntV;
  //! \brief typedef for a vector of strings (to save some typing)
  typedef std::vector<TString> StrV;

  //! \brief Converts a text line to a vector of words
  //  \param str input string with words
  //  \param sep separator to define where a word ends or starts
  StrV vectorize(TString str, TString sep=" ");
  
  //! \brief Converts string of separated numbers to vector<double>
  //  \param str input string with numbers
  //  \param sep separator to define where a number ends or starts
  NumV vectorizeNum(TString str, TString sep=" ");
  
  //! \brief method to abort program with error message
  void fatal(TString msg);
  
  //! \brief returns true if a given file or directory exist
  bool fileExist(TString fn);

  static const double Pi(3.14159265359);
  static const double PiHalf(1.57079632679);

  //! \brief assigns a value to a variable based on auxdata
  template <typename T1, typename T2>
  void assignFromAux(const T1* obj, const char* aux_string, T2& var) {
    if ( obj->template isAvailable<T2>(aux_string) ) {
      var = obj->template auxdata<T2>(aux_string);
    }
    else {
      var = 999999;
    }
  }

  //! \brief returns pointer to truth particle of a track particle, return nullptr if no truth available.
  const xAOD::TruthParticle* getTruthPtr(const xAOD::TrackParticle& trackParticle);

}

inline const xAOD::TruthParticle* TRT::getTruthPtr(const xAOD::TrackParticle& trackParticle) {
  const xAOD::TruthParticle *result = nullptr;
  if ( TRT::truthLink.isAvailable(trackParticle) ) {
    const TruthLink_t link = trackParticle.auxdata<TruthLink_t>("truthParticleLink");
    if ( link.isValid() ) {
      result = *link;
    }
  }
  return result;
}

#endif
