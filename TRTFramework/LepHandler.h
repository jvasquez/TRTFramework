#ifndef TRT_LepHandler
#define TRT_LepHandler

#include <iostream>
#include "TString.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#ifndef __CINT__
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "TRTFramework/TRTUtils.h"
#include "TRTFramework/TRTIncludes.h"
#include "TRTFramework/Config.h"
#endif

namespace TRT {
  class LepHandler {

    protected:
      TString m_name;
      xAOD::TEvent *m_event;
      xAOD::TStore *m_store;
      unsigned int m_nElContainer;
      unsigned int m_nMuContainer;

    private:
      double m_MuonPtCut;
      double m_MuonEtaCut;
      
      double m_ElectronPtCut;
      double m_ElectronEtaCut;
      
      bool   m_useRelTrkCut;
      double m_relTrkPtCut;
      double m_TrackPtCut;
      double m_TrackEtaCut;
      
      bool m_useMaxTrkPtCut;
      double m_maxTrackPtCut;
      
      int m_nIBLBLayCut;
      int m_nPixSCTCut;
      int m_nPixCut;
      int m_nSCTCut;
      int m_nTRTCut;

    public:
      EL::StatusCode initialize( TRT::Config &config );

    #ifndef __CINT__
      LepHandler( xAOD::TEvent *event, xAOD::TStore *store );
      ~LepHandler();

      bool comparePt( xAOD::Electron *a, xAOD::Electron *b ); 
      bool comparePt( xAOD::Muon *a,     xAOD::Muon *b ); 
      
      bool passSelection( xAOD::Electron* );
      bool passSelection( xAOD::Muon* );
      bool passSelection( const xAOD::TrackParticle *trk );

      xAOD::ElectronContainer getElectronContainer();
      xAOD::ElectronContainer getSelectedElectrons();
      xAOD::ElectronContainer getSelectedElectrons( xAOD::ElectronContainer );
      
      xAOD::MuonContainer getMuonContainer();
      xAOD::MuonContainer getSelectedMuons();
      xAOD::MuonContainer getSelectedMuons( xAOD::MuonContainer );
      
      const xAOD::TrackParticle * getGSFTrack( xAOD::Electron* );
      const xAOD::TrackParticle * getGSFTrack( xAOD::Muon* mu ) { return getTrack(mu); }
      const xAOD::TrackParticle * getTrack( xAOD::Electron* );
      const xAOD::TrackParticle * getTrack( xAOD::Muon* );
    #endif
  };

}

#endif // TRT_LepHandler
