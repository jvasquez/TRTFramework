#ifndef TRTFramework_JobHelper_H
#define TRTFramework_JobHelper_H

#include "TRTFramework/TRTAnalysis.h"

#include <EventLoop/StatusCode.h>
#include "EventLoop/OutputStream.h"
#include <EventLoopGrid/GridDriver.h>
#include <EventLoopGrid/PrunDriver.h>

#include "EventLoop/DirectDriver.h"
#include "EventLoop/Job.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "xAODRootAccess/Init.h"
#include <TSystem.h>
#include <iostream>
#include <string>


//! \brief TRT namespace
namespace TRT {

  //! \name   A few general helper methods and definitions
  //@{

  void runJob(TRTAnalysis *alg, int argc, char** argv);
  
  //@}
}

#endif
