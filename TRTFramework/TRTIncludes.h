#ifndef TRTANALYSISEDM_H
#define TRTANALYSISEDM_H

// STL includes
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <cmath>

// ROOT includes
#include <TSystem.h>
#include <TTreeFormula.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TMath.h>
#include <TH1.h>
#include <TH1F.h>
#include <TString.h>

// ROOT includes
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>
#include "PathResolver/PathResolver.h"

// xAOD includes
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"

// GRL & PRW
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgTools/ToolHandle.h"

// TRT includes
#include "TRTFramework/TRTUtils.h"
#include "TRTFramework/Config.h"


namespace TRT {
  // 1*GeV = 1000*MeV
  static const double GeV(1000), invGeV(1.0/GeV);
  
  static const double massEl = 0.511;
  static const double massMu = 105.6; // Use pion mass to compare to athena tool
  static const double massPi = 139.6;

  enum GasType { Xenon = 0, Argon = 1 };
  enum Partition { Barrel = 0, EndcapA = 1, EndcapB = 2 };
  
  enum Channel { Electron = 0, Muon = 1 };
  enum Selection { ZBoson = 0, JPsi = 1, ZZlll = 2 };

}


/// Helper macro to check xAOD::TReturnCode return values
/// See Atilla's slide 8-9 at: https://indico.cern.ch/event/362819/

#define CP_CHECK( COMMENT, EXP )                        \
  do {                                                  \
    if ( EXP != CP::SystematicCode::Ok ) {              \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),  \
             #EXP );                                    \
      return CP::SystematicCode::Unsupported;           \
    }                                                   \
  } while( false );


#define CHECK( EXP )                                             \
  {                                                              \
    if (!EXP.isSuccess())                                        \
      Fatal( "CHECK", "Failed to execute: %s. Exiting.", #EXP);  \
  }


#define MySafeDelete(x) { if (x) { delete x; x=0; } }


#endif //TRTANALYSISEDM_H
