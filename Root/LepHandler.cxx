#include <iostream>
#include "TString.h"
#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCore/ShallowCopy.h"
#include "TRTFramework/TRTUtils.h"
#include "TRTFramework/LepHandler.h"
#include "TRTFramework/Config.h"


namespace TRT {

  bool LepHandler::comparePt( xAOD::Electron *a, xAOD::Electron *b ) { return ( a->pt() > b->pt() ); }
  //---------------------------------------------------------- 

  bool LepHandler::comparePt( xAOD::Muon *a, xAOD::Muon *b ) { return ( a->pt() > b->pt() ); }
  //----------------------------------------------------------


  LepHandler::LepHandler( xAOD::TEvent *event, xAOD::TStore *store)
  : m_event(event)
  , m_store(store)
  , m_nElContainer(0)
  , m_nMuContainer(0)
  { }



  //-------------------
  // Initialization 
  //-------------------

  EL::StatusCode LepHandler::initialize( TRT::Config &config )
  {
      // Electron Requirements
      m_ElectronPtCut  = config.getNum(  "LepHandler.ElectronPtCutGeV",    20 );
      m_ElectronEtaCut = config.getNum(  "LepHandler.ElectronAbsEtaCut",  2.7 );
      
      // Muon Requirements
      m_MuonPtCut      = config.getNum(  "LepHandler.MuonPtCutGeV",        20 );
      m_MuonEtaCut     = config.getNum(  "LepHandler.MuonAbsEtaCut",      2.7 );
      
      // Track Requirements 
      m_useRelTrkCut   = config.getBool( "LepHandler.UseRelativeTrackPtCut", true );
      m_relTrkPtCut    = config.getNum(  "LepHandler.RelativeTrackPtCut",    0.25 );
      
      m_TrackPtCut     = config.getNum(  "LepHandler.TrackPtCutGeV",       20 );
      m_TrackEtaCut    = config.getNum(  "LepHandler.TrackAbsEtaCut",     2.7 );
   
      m_useMaxTrkPtCut = config.getBool( "LepHandler.UseMaxTrackPtCutGeV",  false );
      m_maxTrackPtCut  = config.getNum(  "LepHandler.MaxTrackPtCutGeV",      50. );
      
      m_nIBLBLayCut    = config.getInt(  "LepHandler.nIBLplusBlayCut",      1 );
      m_nPixSCTCut     = config.getInt(  "LepHandler.nPixplusSCTCut",       6 );
      m_nPixCut        = config.getInt(  "LepHandler.nPixCut",             -1 );  
      m_nSCTCut        = config.getInt(  "LepHandler.nSCTCut",             -1 );
      m_nTRTCut        = config.getInt(  "LepHandler.nTRTCut",              5 );

      return EL::StatusCode::SUCCESS;
  }



  //--------------------
  // Electrons 
  //--------------------

  xAOD::ElectronContainer LepHandler::getElectronContainer() {
  //---------------------------------------------------------- 
    //Get the raw Electron Container
    const xAOD::ElectronContainer* electrons = 0; 
    if ( m_event->retrieve( electrons, "Electrons" ).isFailure() ) { 
      TRT::fatal("Failed to retrieve Electrons container."); 
    }

    //create a shallow copy and convert it to an ElectronContainer
    std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > shallowCopy = xAOD::shallowCopyContainer( *electrons );
    xAOD::ElectronContainer container(shallowCopy.first->begin(), 
                                      shallowCopy.first->end(),
                                      SG::VIEW_ELEMENTS);

    //add shallow container to TStore for clean up 
    TString name = TString::Format("ShallowElectrons_%d", m_nElContainer);
    if (!m_store->record(shallowCopy.first,  name.Data())) {    TRT::fatal("Cannot store Electron container in TStore"); }
    name = TString::Format("ShallowElectronsAux_%d", m_nElContainer);
    if (!m_store->record(shallowCopy.second, name.Data())) { TRT::fatal("Cannot store Electron aux container in TStore"); }
    m_nElContainer++;

    //container.sort(comparePt);

    return container;
  }

  bool LepHandler::passSelection( xAOD::Electron *el ) { 
  //----------------------------------------------------------
    if ( el->pt() < m_ElectronPtCut*GeV ) return false;
    if ( fabs(el->eta()) > m_ElectronEtaCut ) return false;

    //Electron ID
    unsigned int isEMLoose = el->auxdata< unsigned int >("isEMLoose");
    if ( isEMLoose != 0 ) return false; //probably not optimal
    
    //Isolation
    //double ptcone20el = el->auxdata< float >("ptcone20");
    //if ( ptcone20el / el->pt() > 0.10 ) return false;

    return true;
  }

  xAOD::ElectronContainer LepHandler::getSelectedElectrons( xAOD::ElectronContainer rawContainer ) {
  //----------------------------------------------------------
    xAOD::ElectronContainer selectedElectrons(SG::VIEW_ELEMENTS);
    for( xAOD::Electron *el : rawContainer ) {
      if( !passSelection(el) ) continue;
      selectedElectrons.push_back(el);
    }
    return selectedElectrons;
  }

  xAOD::ElectronContainer LepHandler::getSelectedElectrons() {
  //----------------------------------------------------------
    xAOD::ElectronContainer rawContainer = getElectronContainer();
    return getSelectedElectrons( rawContainer );
  }



  //--------------------
  // Muons
  //--------------------  

  xAOD::MuonContainer LepHandler::getMuonContainer() {
  //----------------------------------------------------------
    //Get the raw Muon Container
    const xAOD::MuonContainer* muons = 0;
    if ( !m_event->retrieve( muons, "Muons" ).isSuccess() ){ // retrieve arguments: container type, container key
      TRT::fatal("Failed to retrieve Muons container. Exiting.");
    }

    //create a shallow copy and convert it to a MuonContainer
    std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > shallowCopy = xAOD::shallowCopyContainer( *muons );
    xAOD::MuonContainer container(shallowCopy.first->begin(), 
                                      shallowCopy.first->end(),
                                      SG::VIEW_ELEMENTS);

    //add shallow container to TStore for clean up 
    TString name = TString::Format("ShallowMuons_%d", m_nMuContainer);
    if (!m_store->record(shallowCopy.first,  name.Data())) {    TRT::fatal("Cannot store Muons container in TStore"); }
    name = TString::Format("ShallowMuonsAux_%d", m_nMuContainer);
    if (!m_store->record(shallowCopy.second, name.Data())) { TRT::fatal("Cannot store Muons aux container in TStore"); }
    m_nMuContainer++;
    
    //container.sort(comparePt);

    return container;
  }

  bool LepHandler::passSelection( xAOD::Muon *mu ) {
  //----------------------------------------------------------
    if ( mu->pt() < m_MuonPtCut*GeV ) return false;
    if ( fabs(mu->eta()) > m_MuonEtaCut ) return false;
    
    //Muon ID?? 

    //Isolation
    //double ptcone20mu = mu->auxdata< float >("ptcone20");
    //if ( ptcone20mu / mu->pt() > 0.10 ) return false;

    return true;
  }

  xAOD::MuonContainer LepHandler::getSelectedMuons( xAOD::MuonContainer rawContainer ) {
  //----------------------------------------------------------
    xAOD::MuonContainer selectedMuons(SG::VIEW_ELEMENTS);
    for ( xAOD::Muon *mu : rawContainer ) {
      if ( !passSelection(mu) ) continue; 
      selectedMuons.push_back(mu);
    }

    return selectedMuons;
  }

  xAOD::MuonContainer LepHandler::getSelectedMuons() {
  //----------------------------------------------------------
    xAOD::MuonContainer rawContainer = getMuonContainer();
    return getSelectedMuons( rawContainer );
  }



  //--------------------
  // Tracks
  //-------------------- 
  const xAOD::TrackParticle * LepHandler::getGSFTrack( xAOD::Electron *el ) {
  //----------------------------------------------------------
    //return the GSF track and decorate it with the lepton pt
    const xAOD::TrackParticle *gsfTrack = el->trackParticle();
    float pTrk = gsfTrack->charge() / gsfTrack->auxdata<float>("QoverPLM");
    
    // Decorate GSF information to track
    gsfTrack->auxdecor< double >( "GSF_pTrk" ) = pTrk; 
    gsfTrack->auxdecor< double >( "GSF_trkpt" ) = pTrk / cosh( gsfTrack->eta() );

    // Decorate Lepton Information
    gsfTrack->auxdecor< double >( "lepPt" ) = el->pt();

    return gsfTrack;
  }

  const xAOD::TrackParticle * LepHandler::getTrack( xAOD::Electron *el ) {
  //----------------------------------------------------------
    //return the InDet track and decorate it with the lepton pt
    const xAOD::TrackParticle *gsfTrack = getGSFTrack(el);
    if (gsfTrack == nullptr) return nullptr;

    const xAOD::TrackParticle *track = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(gsfTrack);
    if (track == nullptr) {
      std::cerr << "   ERROR :: InDetTrack is null pointer from GSF!!!" << std::endl;
      return nullptr;
    }

    // Decorate GSF information to track
    float pTrk = gsfTrack->charge() / gsfTrack->auxdata<float>("QoverPLM");
    track->auxdecor< double >( "GSF_pTrk" ) = pTrk; 
    track->auxdecor< double >( "GSF_trkpt" ) = pTrk / cosh( gsfTrack->eta() );
    
    // Decorate Lepton Information
    track->auxdecor< double >( "lepPt" ) = el->pt();
    
    return track;
  }

  const xAOD::TrackParticle * LepHandler::getTrack( xAOD::Muon *mu ) {
  //----------------------------------------------------------
    //return the InDet track and decorate it with the lepton pt
    if (!mu->inDetTrackParticleLink().isValid()) return 0;
    const xAOD::TrackParticle *track = *mu->inDetTrackParticleLink();
    
    // Decorate GSF information to track (Not really GSFs...)
    track->auxdecor< double >( "GSF_pTrk" ) = track->pt() * cosh( track->eta() );
    track->auxdecor< double >( "GSF_trkpt" ) = track->pt();

    // Decorate Lepton Information
    track->auxdecor< double >( "lepPt" ) = mu->pt();

    return track;
  }

  bool LepHandler::passSelection( const xAOD::TrackParticle *trk ) {
    if (trk == nullptr) return false;
  
    // Use Relative pT Cuts
    if ( m_useRelTrkCut ) {
      double lepPt = trk->auxdata< double >("lepPt");
      if ( trk->pt() < m_relTrkPtCut*lepPt ) return false;

    // Use Absolute pT Cuts 
    } else {
      if ( trk->pt() < m_TrackPtCut * TRT::GeV ) return false;
    }
    
    if ( m_useMaxTrkPtCut && trk->pt() > m_maxTrackPtCut * TRT::GeV ) return false;
   
    // Require Track |Eta|
    if ( fabs(trk->eta()) > m_TrackEtaCut ) return false;

    // Require Track Hits
    int nIBLhits  = trk->auxdata< unsigned char >("numberOfInnermostPixelLayerHits");
    int nBLayhits = trk->auxdata< unsigned char >("numberOfNextToInnermostPixelLayerHits");
    int nPixhits  = trk->auxdata< unsigned char >("numberOfPixelHits");
    int nSCThits  = trk->auxdata< unsigned char >("numberOfSCTHits");
    int nTRThits  = trk->auxdata< unsigned char >("numberOfTRTHits"); 
    
    if ( ( nIBLhits + nBLayhits ) < m_nIBLBLayCut ) return false;
	  if ( ( nPixhits + nSCThits  ) < m_nPixSCTCut  ) return false;
	  if ( nPixhits < m_nPixCut ) return false;
	  if ( nSCThits < m_nSCTCut ) return false;
	  if ( nTRThits < m_nTRTCut ) return false;

    return true;
  }

}
