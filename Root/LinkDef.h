#include <TRTFramework/TRTAnalysis.h>
#include <TRTFramework/Config.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class TRTAnalysis+;
#pragma link C++ class TRT::Config+;
#endif
