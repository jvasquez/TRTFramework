#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <TRTFramework/TRTAnalysis.h>
#include "TRTFramework/TRTIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TRTAnalysis)


TRTAnalysis :: TRTAnalysis ()
: m_event(nullptr)
, m_store(nullptr)
, m_histoStore(nullptr)
, m_lepHandler(nullptr)  
, m_PRWTool(nullptr)
, m_grl(nullptr)
{
  // Must have no pointer initialization, for CINT
}


EL::StatusCode TRTAnalysis :: setupJob (EL::Job& job)
{
  job.useXAOD();
  
  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init( "TRTAnalysis" ).ignore();
  
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TRTAnalysis :: createOutput() {
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TRTAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  m_histoStore = new HistogramStore();
  
  // run hist prep script
  createOutput();
  
  // register all histograms
  for (auto *histo : histoStore()->getListOfHistograms()) {
     wk()->addOutput(histo);
  }
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  

  // read event tree
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  
  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event()->getEntries() ); // print long long int
  
  // initialize counts for number of events
  m_eventCounter = -1;
  
  // -------------------------
  //  Prepare Config Files
  // -------------------------
  
  // TEnv uses value from first file it's specified in.
  // If specified, read in additional configuration
  if (m_config.isDefined("Include"))
    for (TString cfg : m_config.getStrV("Include"))
      m_config.addFile(cfg);
  
  // Fill unspecified values from default config, specified here.
  if (!m_config.isDefined("BaseConfig")) {
     TRT::fatal("You must specify a base configuration file, option: BaseConfig. Exiting.");
  } else {
     m_config.addFile(m_config.getStr("BaseConfig"));
  }
  
  // Print configuration database, if requested
  if (m_config.getBool("TRTAnalysis.PrintConfig", true)) {
    Info("initialize()", "Printing full configuration:");
    m_config.printDB();
    Info("initialize()", " ");
  }
  
  m_removeOverlap = m_config.getBool("TRTAnalysis.RemoveEventOverlap", true);
  m_usePRW = m_config.getBool("EventHandler.PRW.UsePRW", false);

  
  // -------------------------
  //  Initialize GRL Tool
  // -------------------------
  std::vector<std::string> vecGRL;
  for (auto grl: m_config.getStrV("EventHandler.GRL"))
    vecGRL.push_back(PathResolverFindCalibFile(grl.Data()));

  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  CHECK(m_grl->setProperty("GoodRunsListVec", vecGRL));

  if (m_grl->initialize().isFailure())
    TRT::fatal("Failed to initialize GRL tool");

  // --------------------------
  //  Initialize PRW Tool 
  // --------------------------
  
  if (m_usePRW) { 
    m_prwSF = m_config.getNum("EventHandler.PRW.DataScaleFactor", 0.862069);
    int defChan  = m_config.getNum("EventHandler.PRW.DefaultChannel" , 314000  );
    
    std::vector<std::string> confFiles;
    std::vector<std::string> lcalcFiles;

    for (TString val: m_config.getStrV("EventHandler.PRW.ConfigFiles"))
      confFiles.push_back(val.Data());

    for (TString val: m_config.getStrV("EventHandler.PRW.LumiCalcFiles"))
      lcalcFiles.push_back(val.Data());

    m_PRWTool = new CP::PileupReweightingTool("Pileup");

    CHECK(asg::setProperty(m_PRWTool,"ConfigFiles"    , confFiles ));
    CHECK(asg::setProperty(m_PRWTool,"LumiCalcFiles"  , lcalcFiles));
    CHECK(asg::setProperty(m_PRWTool,"DataScaleFactor", m_prwSF   ));
    CHECK(asg::setProperty(m_PRWTool,"DefaultChannel" , defChan   ));

    if (m_PRWTool->initialize().isFailure())
      TRT::fatal("Failed to initialize PRW tool");
  }
  
  // --------------------------
  //  Initialize Handlers 
  // --------------------------
  
  m_lepHandler = new TRT::LepHandler(event(),store());
  m_lepHandler->initialize(m_config);

  // create some whitespace
  std::cout << std::endl;

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: execute ()
{
  // print processing rate at set interval, so we know where we are:
  static int progressInterval = config()->getInt("OutputMessage.ProcessedEventsInterval",1000);
  
  m_eventCounter++;
  if( m_eventCounter == 0) m_startTime = time(NULL);
  if( m_eventCounter && (m_eventCounter % progressInterval == 0) ) {
    Info("execute()","%i events processed so far  <<<==", m_eventCounter );
    Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/(time(NULL)-m_startTime));
  } 
  
  // Apply PileUp Reweighting 
  if (isMC() && m_usePRW) 
    CHECK( m_PRWTool->apply(*eventInfo()) );

  // Get Event Weights 
  m_WeightAll = (isData()) ? 1.0 : mcWeight() * pileupWeight();  
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  MySafeDelete(m_grl);
  MySafeDelete(m_PRWTool);
  MySafeDelete(m_histoStore);
  //MySafeDelete(m_lepHandler);
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TRTAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  Info("","Completed running over %d events", m_eventCounter);
  return EL::StatusCode::SUCCESS;
}
